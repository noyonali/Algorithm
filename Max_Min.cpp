#include <iostream>
using namespace std;
int main()
{
    int arr[100];
    int i, max, min, size;

    cout<<"Enter size of the array: ";
    cin>>size;
    cout<<endl;

    for(i=0; i<size; i++)
    {
        cout<<"Enter number in the array "<<i+1 <<" :";
        cin>>arr[i];
    }

    max = arr[0];
    min = arr[0];


    for(i=1; i<size; i++)
    {
        // max number
        if(arr[i] > max)
        {
            max = arr[i];
        }

        // min number
        if(arr[i] < min)
        {
            min = arr[i];
        }
    }

    // Print maximum and minimum number
    cout<<"Maximum number = " <<max <<endl;
    cout<<"Minimum number = " << min <<endl;
    cout<<endl;
    cout<<"Md.Noyon Ali" <<endl;
    cout<<"ID:2018200010027"<<endl;

    return 0;
}
