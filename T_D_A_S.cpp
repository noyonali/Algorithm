#include <iostream>
using namespace std;
int main()
{
    int r, c, firstMatrix[100][100], secondMatrix[100][100], sum[100][100], i, j;
    cout << "Enter Number Of Rows (1 To 100): ";
    cin >> r;
    cout << "Enter Number of columns (1 to 100): ";
    cin >> c;
    cout << endl << "Enter Elements Of First matrix: " << endl;
    //First matrix entered by user
    for(i = 0; i < r; i++)
    {
       for(j = 0; j < c; j++)
       {
           cout << "Enter Element First Matrix" << i + 1 << j + 1 << " : ";
           cin >> firstMatrix[i][j];
       }
    }
   //Second matrix entered by user
    cout << endl << "Enter Elements Of Second Matrix: " << endl;
    for(i = 0; i < r; i++)
    {
       for(j = 0; j < c; j++)
       {
           cout << "Enter Element Second Matrix" << i + 1 << j + 1 << " : ";
           cin >> secondMatrix[i][j];
       }
    }
    // Adding Two matrices
    for(i = 0; i < r; i++)
    {
        for(j = 0; j < c; j++)
        {
            sum[i][j] = firstMatrix[i][j] + secondMatrix[i][j];
        }
    }
    // sum  of two dimensional matrix  display
    cout<<"Sum of the two matrices is:"<<endl;
   for(i=0; i<r; i++) {
      for(j=0; j<c; j++){
         cout<<sum[i][j]<<" ";
         }
      cout<<endl;
   }
   cout<<endl <<"Md.Noyon Ali"<<endl;
   cout<<"ID:2018200010027"<<endl;
   return 0;
}
